#!/bin/bash
echo "Start script execution---->>05-install-tomcat-9.sh<----"

wget -nv https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.73/bin/apache-tomcat-9.0.73.tar.gz -O /tmp/tomcat.tgz && \
tar xzf /tmp/tomcat.tgz -C /opt && \
mv /opt/apache-tomcat-9.0.73 /opt/tomcat && \
rm /tmp/tomcat.tgz

echo "End script execution---->>05-install-tomcat-9.sh<<----"